"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""
import builtins
import typing

import google.protobuf.descriptor
import google.protobuf.internal.enum_type_wrapper
import google.protobuf.message
import typing_extensions

import SiLAFramework_pb2

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor = ...

class CreateBinaryRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYSIZE_FIELD_NUMBER: builtins.int
    CHUNKCOUNT_FIELD_NUMBER: builtins.int
    PARAMETERIDENTIFIER_FIELD_NUMBER: builtins.int
    binarySize: builtins.int = ...
    chunkCount: builtins.int = ...
    parameterIdentifier: typing.Text = ...
    def __init__(
        self,
        *,
        binarySize: builtins.int = ...,
        chunkCount: builtins.int = ...,
        parameterIdentifier: typing.Text = ...,
    ) -> None: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binarySize", b"binarySize", "chunkCount", b"chunkCount", "parameterIdentifier", b"parameterIdentifier"
        ],
    ) -> None: ...

global___CreateBinaryRequest = CreateBinaryRequest

class CreateBinaryResponse(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    LIFETIMEOFBINARY_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    @property
    def lifetimeOfBinary(self) -> SiLAFramework_pb2.Duration: ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
        lifetimeOfBinary: typing.Optional[SiLAFramework_pb2.Duration] = ...,
    ) -> None: ...
    def HasField(
        self, field_name: typing_extensions.Literal["lifetimeOfBinary", b"lifetimeOfBinary"]
    ) -> builtins.bool: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binaryTransferUUID", b"binaryTransferUUID", "lifetimeOfBinary", b"lifetimeOfBinary"
        ],
    ) -> None: ...

global___CreateBinaryResponse = CreateBinaryResponse

class UploadChunkRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    CHUNKINDEX_FIELD_NUMBER: builtins.int
    PAYLOAD_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    chunkIndex: builtins.int = ...
    payload: builtins.bytes = ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
        chunkIndex: builtins.int = ...,
        payload: builtins.bytes = ...,
    ) -> None: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binaryTransferUUID", b"binaryTransferUUID", "chunkIndex", b"chunkIndex", "payload", b"payload"
        ],
    ) -> None: ...

global___UploadChunkRequest = UploadChunkRequest

class UploadChunkResponse(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    CHUNKINDEX_FIELD_NUMBER: builtins.int
    LIFETIMEOFBINARY_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    chunkIndex: builtins.int = ...
    @property
    def lifetimeOfBinary(self) -> SiLAFramework_pb2.Duration: ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
        chunkIndex: builtins.int = ...,
        lifetimeOfBinary: typing.Optional[SiLAFramework_pb2.Duration] = ...,
    ) -> None: ...
    def HasField(
        self, field_name: typing_extensions.Literal["lifetimeOfBinary", b"lifetimeOfBinary"]
    ) -> builtins.bool: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binaryTransferUUID",
            b"binaryTransferUUID",
            "chunkIndex",
            b"chunkIndex",
            "lifetimeOfBinary",
            b"lifetimeOfBinary",
        ],
    ) -> None: ...

global___UploadChunkResponse = UploadChunkResponse

class DeleteBinaryRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
    ) -> None: ...
    def ClearField(
        self, field_name: typing_extensions.Literal["binaryTransferUUID", b"binaryTransferUUID"]
    ) -> None: ...

global___DeleteBinaryRequest = DeleteBinaryRequest

class DeleteBinaryResponse(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    def __init__(
        self,
    ) -> None: ...

global___DeleteBinaryResponse = DeleteBinaryResponse

class GetBinaryInfoRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
    ) -> None: ...
    def ClearField(
        self, field_name: typing_extensions.Literal["binaryTransferUUID", b"binaryTransferUUID"]
    ) -> None: ...

global___GetBinaryInfoRequest = GetBinaryInfoRequest

class GetBinaryInfoResponse(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYSIZE_FIELD_NUMBER: builtins.int
    LIFETIMEOFBINARY_FIELD_NUMBER: builtins.int
    binarySize: builtins.int = ...
    @property
    def lifetimeOfBinary(self) -> SiLAFramework_pb2.Duration: ...
    def __init__(
        self,
        *,
        binarySize: builtins.int = ...,
        lifetimeOfBinary: typing.Optional[SiLAFramework_pb2.Duration] = ...,
    ) -> None: ...
    def HasField(
        self, field_name: typing_extensions.Literal["lifetimeOfBinary", b"lifetimeOfBinary"]
    ) -> builtins.bool: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal["binarySize", b"binarySize", "lifetimeOfBinary", b"lifetimeOfBinary"],
    ) -> None: ...

global___GetBinaryInfoResponse = GetBinaryInfoResponse

class GetChunkRequest(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    OFFSET_FIELD_NUMBER: builtins.int
    LENGTH_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    offset: builtins.int = ...
    length: builtins.int = ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
        offset: builtins.int = ...,
        length: builtins.int = ...,
    ) -> None: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binaryTransferUUID", b"binaryTransferUUID", "length", b"length", "offset", b"offset"
        ],
    ) -> None: ...

global___GetChunkRequest = GetChunkRequest

class GetChunkResponse(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...
    BINARYTRANSFERUUID_FIELD_NUMBER: builtins.int
    OFFSET_FIELD_NUMBER: builtins.int
    PAYLOAD_FIELD_NUMBER: builtins.int
    LIFETIMEOFBINARY_FIELD_NUMBER: builtins.int
    binaryTransferUUID: typing.Text = ...
    offset: builtins.int = ...
    payload: builtins.bytes = ...
    @property
    def lifetimeOfBinary(self) -> SiLAFramework_pb2.Duration: ...
    def __init__(
        self,
        *,
        binaryTransferUUID: typing.Text = ...,
        offset: builtins.int = ...,
        payload: builtins.bytes = ...,
        lifetimeOfBinary: typing.Optional[SiLAFramework_pb2.Duration] = ...,
    ) -> None: ...
    def HasField(
        self, field_name: typing_extensions.Literal["lifetimeOfBinary", b"lifetimeOfBinary"]
    ) -> builtins.bool: ...
    def ClearField(
        self,
        field_name: typing_extensions.Literal[
            "binaryTransferUUID",
            b"binaryTransferUUID",
            "lifetimeOfBinary",
            b"lifetimeOfBinary",
            "offset",
            b"offset",
            "payload",
            b"payload",
        ],
    ) -> None: ...

global___GetChunkResponse = GetChunkResponse

class BinaryTransferError(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor = ...

    class ErrorType(_ErrorType, metaclass=_ErrorTypeEnumTypeWrapper):
        pass

    class _ErrorType:
        V = typing.NewType("V", builtins.int)

    class _ErrorTypeEnumTypeWrapper(
        google.protobuf.internal.enum_type_wrapper._EnumTypeWrapper[_ErrorType.V], builtins.type
    ):
        DESCRIPTOR: google.protobuf.descriptor.EnumDescriptor = ...
        INVALID_BINARY_TRANSFER_UUID = BinaryTransferError.ErrorType.V(0)
        BINARY_UPLOAD_FAILED = BinaryTransferError.ErrorType.V(1)
        BINARY_DOWNLOAD_FAILED = BinaryTransferError.ErrorType.V(2)
    INVALID_BINARY_TRANSFER_UUID = BinaryTransferError.ErrorType.V(0)
    BINARY_UPLOAD_FAILED = BinaryTransferError.ErrorType.V(1)
    BINARY_DOWNLOAD_FAILED = BinaryTransferError.ErrorType.V(2)

    ERRORTYPE_FIELD_NUMBER: builtins.int
    MESSAGE_FIELD_NUMBER: builtins.int
    errorType: global___BinaryTransferError.ErrorType.V = ...
    message: typing.Text = ...
    def __init__(
        self,
        *,
        errorType: global___BinaryTransferError.ErrorType.V = ...,
        message: typing.Text = ...,
    ) -> None: ...
    def ClearField(
        self, field_name: typing_extensions.Literal["errorType", b"errorType", "message", b"message"]
    ) -> None: ...

global___BinaryTransferError = BinaryTransferError
