``sila2-codegen generate-feature-files``: Generate feature-specific files without a package
===========================================================================================

If you don't want to use the package structure utilized by the other code generation commands and instead only want to generate the feature-spefific code,
use this command.

.. runcmd:: sila2-codegen generate-feature-files --help
