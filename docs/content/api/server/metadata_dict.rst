Class MetadataDict
=========================

.. autoclass:: sila2.server.MetadataDict
    :show-inheritance:
    :no-inherited-members:
