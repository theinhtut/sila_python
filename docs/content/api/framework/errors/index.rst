Error API
=========

Base classes
------------

.. autoexception:: sila2.framework.SilaError
    :show-inheritance:


.. autoexception:: sila2.framework.FrameworkError
    :show-inheritance:

Errors raised by SiLA Clients
-----------------------------

.. autoexception:: sila2.framework.SilaConnectionError
    :show-inheritance:

Errors raised by SiLA Servers
-----------------------------

.. autoexception:: sila2.framework.DefinedExecutionError
    :show-inheritance:

.. autoexception:: sila2.framework.ValidationError
    :show-inheritance:

.. autoexception:: sila2.framework.UndefinedExecutionError
    :show-inheritance:

    .. automethod:: sila2.framework.UndefinedExecutionError.from_exception

.. autoexception:: sila2.framework.CommandExecutionNotAccepted
    :show-inheritance:

.. autoexception:: sila2.framework.CommandExecutionNotFinished
    :show-inheritance:

.. autoexception:: sila2.framework.InvalidMetadata
    :show-inheritance:

.. autoexception:: sila2.framework.NoMetadataAllowed
    :show-inheritance:

.. autoexception:: sila2.framework.InvalidCommandExecutionUUID
    :show-inheritance:
