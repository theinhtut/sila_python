Class FullyQualifiedIdentifier
==============================

.. autoclass:: sila2.framework.FullyQualifiedIdentifier
    :show-inheritance:
    :no-inherited-members:
