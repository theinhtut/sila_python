Using auto-generated SiLA Client classes
========================================

When generating a Python package using the :doc:`/content/code_generator/index`, the generated package will contain a class ``Client``,
which is a subclass of :py:class:`~sila2.client.SilaClient`.

These classes differ from the generic :py:class:`~sila2.client.SilaClient` in the following ways:

- They can only be connected to a SiLA Server implementing at least the features of the generated package
- When receiving a :py:class:`~sila2.framework.DefinedExecutionError` from the SiLA Server, it will be raised as an instance of the generated :py:class:`~sila2.framework.DefinedExecutionError` subclass
    - **Example**: :py:func:`SiLAService.GetFeatureDefinition <sila2.features.silaservice.SiLAServiceClient.GetFeatureDefinition>` will raise an instance of :py:class:`~sila2.features.silaservice.UnimplementedFeature`
- They provide `PEP 484 <https://www.python.org/dev/peps/pep-0484/>`_ type hints so you can use tools like `mypy <https://github.com/python/mypy>`_ to perform static type checking on your client-side code
