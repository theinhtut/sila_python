SiLA Data Types
===============

This section describes how SiLA data types are mapped to Python types.

Basic types
-----------

- **Integer**: :py:class:`int`, a :py:class:`ValueError` will be raised for values < -2^63 or > 2^63-1
- **Real**: :py:class:`float`
- **Boolean**: :py:class:`bool`
- **String**: :py:class:`string`, a :py:class:`ValueError` will be raised if the length exceeds 2^21
- **Binary**: :py:class:`bytes`, for values > 2 MB both sides of the communication must support SiLA Binary Transfer (which this SDK does)
- **Date**: :py:class:`~sila2.framework.SilaDateType`, a :py:class:`~typing.NamedTuple` class with the fields ``date`` and ``timezone`` (:py:class:`~datetime.date`, :py:class:`~datetime.timezone`)
- **Time**: :py:class:`~datetime.time`, a :py:class:`ValueError` will be raised if the :py:attr:`~datetime.time.tzinfo` attribute is ``None``
- **Timestamp**: :py:class:`~datetime.datetime`, a :py:class:`ValueError` will be raised if the :py:attr:`~datetime.datetime.tzinfo` attribute is ``None``
- **Any**: :py:class:`~sila2.framework.SilaAnyType`, a :py:class:`~typing.NamedTuple` class with the fields ``type_xml`` and ``value`` (:py:class:`str`, :py:data:`~typing.Any`)

List
----

The **List** type is mapped to a Python :py:class:`list`.

Constrained
-----------

The **Constrained** type is handled like its base type.
When a SiLA Server implemented using this SDK receives a value of a **Constrained** type as a command parameter,
it will validate the value and raise a :py:class:`~sila2.framework.ValidationError` if the value does not satisfy all constraints.

Structure
---------

**Structures** are mapped implicitly to named tuples.
These are currently not supported by the code generator, so no appropriate :py:class:`~typing.NamedTuple` classes are generated yet.
The SDK will interpret plain :py:class:`tuple` objects as structures, if it has the proper number and types of items.

Example
^^^^^^^

A data type ``Point2D`` is defined like this:

.. code-block:: xml

    <DataType>
        <Structure>
            <Element>
                <Identifier>X</Identifier>
                <DisplayName>X</DisplayName>
                <Description>The X coordinate</Description>
                <DataType>
                    <Basic>Integer</Basic>
                </DataType>
            </Element>
            <Element>
                <Identifier>Y</Identifier>
                <DisplayName>Y</DisplayName>
                <Description>The Y coordinate</Description>
                <DataType>
                    <Basic>Integer</Basic>
                </DataType>
            </Element>
        </Structure>
    </DataType>

The implicitly created class is:

.. code-block:: python

    from typing import NamedTuple

    class Point2D_Struct(NamedTuple):
        X: int
        Y: int

When you receives a ``Point2D`` object, you can access the structure elements like this:

    >>> point = client.CoordinateFeature.Create2DPoint(1, 2)  # returns Point2D(1, 2)

    >>> # tuple indexing
    >>> x = point[0]
    >>> y = point[1]

    >>> # attributes
    >>> x = point.X
    >>> y = point.Y

    >>> # iterable unpacking
    >>> x, y = point

When expecting a **Structure**, the SDK will accept a plain :py:class:`tuple` containing two :py:class:`int` objects: ``(1, 2)``.
