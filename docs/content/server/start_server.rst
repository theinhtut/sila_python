Start the generated SiLA Server
===============================

The code snippets use the package name ``my_sila2_package``, as in the example :doc:`here <code_generator>`.
Use the name of your package instead.

Install the package
-------------------

After you :doc:`generated a SiLA Server/Client package <code_generator>`, you can install it with ``pip install .`` from the directory where the file ``setup.cfg`` is located.
Use ``pip install -e .`` instead, if you plan to further modify the package files (this will install the package in "editable" mode).

Run SiLA Server as module
-------------------------

The generated package contains a file ``__main__.py``, which makes it an executable Python module.
You can start the SiLA Server with ``python -m my_sila2_package [options]``.
Two options are required: ``--ip-address`` and ``--port``.
To run the server on your local device ("localhost") and port 50052, use ``python -m my_sila2_package --ip-address 127.0.0.1 --port 50052``.
This will start the server using encrypted communication, using a self-signed certificate.

Run ``python -m my_sila2_package --help`` to see all available options, e.g. on how to use your own certificates, or disable encrypted communication:

.. code-block::

    usage: my_sila2_package [-h] [-a IP_ADDRESS] [-p PORT]
                            [--server-uuid SERVER_UUID] [--disable-discovery]
                            [--insecure] [-k PRIVATE_KEY_FILE] [-c CERT_FILE]
                            [--ca-export-file CA_EXPORT_FILE] [-q | -v | -d]

    Start this SiLA 2 server

    optional arguments:
      -h, --help            show this help message and exit
      -a IP_ADDRESS, --ip-address IP_ADDRESS
                            The IP address (default: '127.0.0.1')
      -p PORT, --port PORT  The port (default: 50052)
      --server-uuid SERVER_UUID
                            The server UUID (default: create random UUID)
      --disable-discovery   Disable SiLA Server Discovery
      --insecure            Start without encryption
      -k PRIVATE_KEY_FILE, --private-key-file PRIVATE_KEY_FILE
                            Private key file (e.g. 'server-key.pem')
      -c CERT_FILE, --cert-file CERT_FILE
                            Certificate file (e.g. 'server-cert.pem')
      --ca-export-file CA_EXPORT_FILE
                            When using a self-signed certificate, write the
                            generated CA to this file
      -q, --quiet           Only log errors
      -v, --verbose         Enable verbose logging
      -d, --debug           Enable debug logging

Run SiLA Server from Python
---------------------------

You can also run the server from within Python.
Just import the ``Server`` class from your package, instantiate it and use the :py:func:`~sila2.server.SilaServer.start` method, which has similar parameters as when executing the package:

.. code-block:: python

    from my_sila2_package import Server

    server = Server()

    try:
        server.start("127.0.0.1", 50052)
        # do something
    finally:
        server.stop()

For details on how to start the server, see the documentation of the :py:func:`~sila2.server.SilaServer.start` and :py:func:`~sila2.server.SilaServer.start_insecure` methods.
