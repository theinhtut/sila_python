import pytest


def test_to_native_type(basic_feature):
    boolean_field = basic_feature._data_type_definitions["Bool"].data_type
    SilaBoolean = boolean_field.message_type

    msg = SilaBoolean(value=False)
    val = boolean_field.to_native_type(msg)

    assert isinstance(val, bool)
    assert not val


def test_to_message(basic_feature):
    boolean_field = basic_feature._data_type_definitions["Bool"].data_type
    SilaBoolean = boolean_field.message_type

    msg = boolean_field.to_message(True)

    assert isinstance(msg, SilaBoolean)
    assert msg.value


def test_wrong_type(basic_feature):
    boolean_field = basic_feature._data_type_definitions["Bool"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        boolean_field.to_message(1)
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        boolean_field.to_message("")
