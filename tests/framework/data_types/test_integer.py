import pytest


def test_to_native_type(basic_feature):
    integer_field = basic_feature._data_type_definitions["Int"].data_type
    SilaInteger = integer_field.message_type

    msg = SilaInteger(value=10)
    val = integer_field.to_native_type(msg)

    assert isinstance(val, int)
    assert val == 10


def test_to_message(basic_feature):
    integer_field = basic_feature._data_type_definitions["Int"].data_type
    SilaInteger = integer_field.message_type

    msg = integer_field.to_message(10)

    assert isinstance(msg, SilaInteger)
    assert msg.value == 10


def test_wrong_type(basic_feature):
    integer_field = basic_feature._data_type_definitions["Int"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        integer_field.to_message(1.0)


def test_out_of_range(basic_feature):
    integer_field = basic_feature._data_type_definitions["Int"].data_type

    _ = integer_field.to_message(2**63 - 1)
    with pytest.raises(ValueError):
        integer_field.to_message(2**63 + 1)

    _ = integer_field.to_message(-1 * 2**63)
    with pytest.raises(ValueError):
        integer_field.to_message(-1 * 2**63 - 1)


def test_from_string(basic_feature):
    integer_field = basic_feature._data_type_definitions["Int"].data_type

    assert integer_field.from_string("1") == 1
    with pytest.raises(ValueError):
        _ = integer_field.from_string("1.")
