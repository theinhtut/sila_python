import pytest


def test_to_native_type(basic_feature):
    binary_field = basic_feature._data_type_definitions["Bytes"].data_type
    SilaBinary = binary_field.message_type

    msg = SilaBinary(value=b"abc")
    val = binary_field.to_native_type(msg)

    assert isinstance(val, bytes)
    assert val == b"abc"


def test_to_message(basic_feature):
    binary_field = basic_feature._data_type_definitions["Bytes"].data_type
    SilaBinary = binary_field.message_type

    msg = binary_field.to_message(b"abc")

    assert isinstance(msg, SilaBinary)
    assert msg.value == b"abc"


def test_wrong_type(basic_feature):
    binary_field = basic_feature._data_type_definitions["Bytes"].data_type

    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        binary_field.to_message(1)
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        binary_field.to_message("")
    with pytest.raises(ValueError):
        binary_field.to_native_type(binary_field.message_type())
