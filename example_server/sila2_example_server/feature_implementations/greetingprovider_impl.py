import datetime

from sila2_example_server.generated.greetingprovider import GreetingProviderBase, SayHello_Responses

from sila2.server import MetadataDict


class GreetingProviderImpl(GreetingProviderBase):
    def SayHello(self, Name: str, *, metadata: MetadataDict) -> SayHello_Responses:
        return SayHello_Responses(f"Hello SiLA 2 {Name}")

    def get_StartYear(self, *, metadata: MetadataDict) -> int:
        return datetime.datetime.now().year
