from __future__ import annotations

import datetime
import time
from typing import TYPE_CHECKING

from sila2_example_server.generated.timerprovider import CountdownTooLong

from sila2.framework import CommandExecutionStatus
from sila2.server import MetadataDict, ObservableCommandInstanceWithIntermediateResponses

from ..generated.timerprovider import Countdown_IntermediateResponses, Countdown_Responses, TimerProviderBase

if TYPE_CHECKING:
    from sila2.server import SilaServer


class TimerProviderImpl(TimerProviderBase):
    def __init__(self, parent_server: SilaServer) -> None:
        super().__init__(parent_server=parent_server)
        self.run_periodically(
            lambda: self.update_CurrentTime(datetime.datetime.now(tz=datetime.timezone.utc).timetz()), delay_seconds=1
        )

    def Countdown(
        self,
        N: int,
        Message: str,
        *,
        metadata: MetadataDict,
        instance: ObservableCommandInstanceWithIntermediateResponses[Countdown_IntermediateResponses],
    ) -> Countdown_Responses:
        # send first info immediately
        instance.status = CommandExecutionStatus.running
        instance.progress = 0
        instance.estimated_remaining_time = datetime.timedelta(seconds=N)

        if N > 9000:
            raise CountdownTooLong

        for i in range(N, 0, -1):
            instance.send_intermediate_response(Countdown_IntermediateResponses(i))
            instance.progress = (N - i) / N * 100
            instance.estimated_remaining_time = datetime.timedelta(seconds=i)

            time.sleep(1)

        return Countdown_Responses(Message, datetime.datetime.now(tz=datetime.timezone.utc))
